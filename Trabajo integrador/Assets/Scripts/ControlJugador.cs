using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    public LayerMask capaPiso;
    public float magnitudSalto;
    public CapsuleCollider col;
    public int maxSaltos = 1;
    public int saltosRestantes;
    public Camera camaraPrimeraPersona;
    public GameObject ItemVelocidad;
   


    Rigidbody rb;


    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;


        
        rb = GetComponent<Rigidbody>();
        col = GetComponent<CapsuleCollider>();


    }

    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;

        }




        if ((Input.GetKeyDown(KeyCode.Space)) && saltosRestantes > 0)
        {
            saltosRestantes -= 1;
            rb.AddForce(Vector3.up * magnitudSalto, ForceMode.Impulse);
            
           
        }

        if (EstaEnPiso())
        {
         
            saltosRestantes = maxSaltos;
        }


        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            if ((Physics.Raycast(ray, out hit) == true) && hit.distance < 30)
            {
                Debug.Log("El rayo toc� al objeto: " + hit.collider.name);
            }

        }


    }



    private void OnTriggerEnter(Collider other)
    {
       

        if (other.gameObject.CompareTag("Velocidad") == true)
        {
            rapidezDesplazamiento = 20.0f;
            other.gameObject.SetActive(false);
        }
    }

    private bool EstaEnPiso()
    {

        return Physics.CheckCapsule(col.bounds.center, new Vector3(col.bounds.center.x,
        col.bounds.min.y, col.bounds.center.z), col.radius * .9f, capaPiso);
    }


}
